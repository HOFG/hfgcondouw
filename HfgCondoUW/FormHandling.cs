﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WinForms = System.Windows.Forms;
using System.Threading.Tasks;
using EllieMae.Encompass.Forms;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects.Users;

namespace HfgCondoUW
{
    public class FormHandling
    {
        StringBuilder message = new StringBuilder();
        private InputForm inputForm { get; set; }

        public FormHandling(InputForm inputForm)
        {
            this.inputForm = inputForm;
        }

        public void FormLoad()
        {
            HandleSubmitRequestButton();
            HandleOtherButtons();
        }

        private void HandleOtherButtons()
        {
            Persona condoUw = EncompassApplication.Session.Users.Personas.GetPersonaByName("Underwriter - Condo");

            if (EncompassApplication.CurrentUser.Personas.Contains(condoUw) == true)
            {
                inputForm.btnSendBack.Enabled = true;
                inputForm.btnComplete.Enabled = true;
                inputForm.btnUnlock.Enabled = true;
                inputForm.btnLock.Enabled = true;
                inputForm.gbReviewInfo.Enabled = true;
            }
        }

        public void HandleSubmitRequestButton()
        {
            if (EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.status].FormattedValue.Equals("TRUE") == true)
            {
                inputForm.btnSubmit.Enabled = false;
            }
            else
            {
                inputForm.btnSubmit.Enabled = true;
            }
        }

        public void HandleSendBackButton()
        {
            EnableSendBackButton();
        }

        public void DisableForm()
        {
            inputForm.gbTypeReview.Enabled = false;
        }

        public void EnableForm()
        {
            inputForm.gbReviewInfo.Enabled = true;
            inputForm.gbTypeReview.Enabled = true;
        }

        private void EnableSendBackButton()
        {
            Persona condoUw = EncompassApplication.Session.Users.Personas.GetPersonaByName("Underwriter - Condo");

            if (EncompassApplication.CurrentUser.Personas.Contains(condoUw) == true)
            {
                inputForm.btnSendBack.Enabled = true;
            }
        }

        public void DisableSendBackButton()
        {
            inputForm.btnSendBack.Enabled = false;
        }

        public void FormControlUnlock()
        {
            inputForm.gbComplete.Enabled = true;
            inputForm.gbReviewInfo.Enabled = true;
            inputForm.gbSubReview.Enabled = true;
            inputForm.gbTypeReview.Enabled = true;
            inputForm.btnSubmit.Enabled = true;
            inputForm.btnSendBack.Enabled = true;
            inputForm.btnComplete.Enabled = true;
        }

        public void FormControlLock()
        {
            inputForm.gbComplete.Enabled = false;
            inputForm.gbReviewInfo.Enabled = false;
            inputForm.gbSubReview.Enabled = false;
            inputForm.gbTypeReview.Enabled = false;
            inputForm.btnSubmit.Enabled = false;
            inputForm.btnSendBack.Enabled = false;
            inputForm.btnComplete.Enabled = false;
        }
    }
}
