﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects;
using EllieMae.Encompass.BusinessObjects.Loans;

namespace HfgCondoUW
{
    public class Complete
    {
        private InputForm inputForm { get; set; }
        private FormHandling formHandling { get; set; }

        public Complete(InputForm inputForm)
        {
            this.formHandling = new FormHandling(inputForm);
        }

        public void ProcessComplete()
        {
            DateTime requestDateTime = ConvertTime.ConvertToArizonaTime(DateTime.Now);
            EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.completedBy].Value = EncompassApplication.CurrentUser.FullName;
            EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.completedDate].Value = requestDateTime.ToShortDateString();
            formHandling.FormControlLock();
            MessageBox.Show("Condo Underwriting request has been completed.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

    }
}
