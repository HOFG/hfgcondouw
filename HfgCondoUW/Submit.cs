﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects;
using EllieMae.Encompass.BusinessObjects.Loans;

namespace HfgCondoUW
{
    public class Submit
    {
        private FormHandling formHandling { get; set; }
        private InputForm inputForm { get; set; }
        private Thread saveThread;
        private bool IsValidRequest = true;
        private string Message = "In order to submit your request please complete the following: \n";
        private int historyNumber = 0;

        public Submit(InputForm inputForm)
        {
            this.formHandling = new FormHandling(inputForm);
            SetHistoryNumber();
        }

        private void SetHistoryNumber()
        {
            for (int index = 1; index <= 5; index++)
            {
                if (string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["cx.cuw.h." + index + ".1"].UnformattedValue) == true)
                {
                    historyNumber = index;
                    break;
                }
            }
        }

        public void SubmitRequest()
        {
            Validation validation = new Validation();
            Message += validation.Validate();
            SetIsValid(validation.isValid);
            ProcessRequest();
        }

        public void SetIsValid(bool result)
        {
            if (result == false)
            {
                IsValidRequest = false;
            }
        }

        public void ProcessRequest()
        {
            if (IsValidRequest == true)
            {
                if (historyNumber > 0)
                {
                    if (IsInitialSubmission() == true)
                    {
                        DateTime requestDateTime = ConvertTime.ConvertToArizonaTime(DateTime.Now);
                        EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submitTime].Value = requestDateTime.ToString("HH:mm:ss");
                        EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submittedByDate].Value = requestDateTime;
                        EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submittedBy].Value = EncompassApplication.CurrentUser.FullName;
                        EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.status].Value = "TRUE";
                        EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submissionType].Value = "INITIAL";
                    }
                    else
                    {
                        DateTime requestDateTime = ConvertTime.ConvertToArizonaTime(DateTime.Now);
                        EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submitTime].Value = requestDateTime.ToString("HH:mm:ss");
                        EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submittedByDate].Value = requestDateTime;
                        EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submittedBy].Value = EncompassApplication.CurrentUser.FullName;
                        EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.status].Value = "TRUE";
                        EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submissionType].Value = "RESUBMISSION";
                    }

                    EncompassApplication.CurrentLoan.Fields["cx.cuw.h." + historyNumber + ".1"].Value = EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submittedBy].Value;
                    EncompassApplication.CurrentLoan.Fields["cx.cuw.h." + historyNumber + ".2"].Value = EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submittedByDate].Value;
                    EncompassApplication.CurrentLoan.Fields["cx.cuw.h." + historyNumber + ".5"].Value = EncompassApplication.CurrentLoan.Fields[FC.Fields.Comments.comments].Value;
                    formHandling.HandleSubmitRequestButton();
                    formHandling.HandleSendBackButton();
                    formHandling.DisableForm();
                    saveThread = new Thread(EncompassApplication.CurrentLoan.Commit);
                    saveThread.Start();
                    MessageBox.Show("Your Condo Underwriting request has been submitted. The loan file has been saved and the input form is now view only.", "Success",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("History records are full. Action canceled.", "History Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(Message, "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private bool IsInitialSubmission()
        {
            if (string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.numResubs].UnformattedValue) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
