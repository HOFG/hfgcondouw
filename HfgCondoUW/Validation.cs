﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects;
using EllieMae.Encompass.BusinessObjects.Loans;

namespace HfgCondoUW
{
    public class Validation
    {
        public bool isValid = false;
        public bool typeResult = false;
        public string Message { get; set; }

        public Validation()
        {
            //
        }

        public string Validate()
        {
            IsTypeReviewValid();
            return Message;
        }

        public void IsTypeReviewValid()
        {
            for (int index = 1; index <= 12; index++)
            {
                if (string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["cx.cuw.tr." + index].UnformattedValue) == false)
                {
                    typeResult = true;
                    break;
                }
            }

            if (typeResult == false)
            {
                Message += "\nType of Review";
                isValid = false;
            }
            else
            {
                isValid = true;
            }

            if (EncompassApplication.CurrentLoan.Fields[FC.Fields.FormFields.typeReview12].FormattedValue.Equals("X") == true)
            {
                if (string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields[FC.Fields.Comments.comments].UnformattedValue) == true)
                {
                    isValid = false;
                    Message += "\nComments for Condo Underwriter";
                }
            }
        }
    }
}
