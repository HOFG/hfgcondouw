﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HfgCondoUW
{
    public static class ConvertTime
    {
        public static DateTime ConvertToArizonaTime(DateTime dateTimeToConvert)
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dateTimeToConvert, "US Mountain Standard Time");
        }
    }
}
