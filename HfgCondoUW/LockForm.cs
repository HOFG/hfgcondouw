﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EllieMae.Encompass.Forms;
using EllieMae.Encompass.Automation;

namespace HfgCondoUW
{
    public class LockForm
    {
        private InputForm inputForm { get; set; }
        private FormHandling formHandling { get; set; }

        public LockForm(InputForm inputForm)
        {
            this.formHandling = new FormHandling(inputForm);
        }

        public void HandleLockFormButton()
        {
            formHandling.FormControlLock();
        }
    }
}
