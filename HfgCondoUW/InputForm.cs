﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EllieMae.Encompass.Forms;

namespace HfgCondoUW
{
    public class InputForm : Form
    {
        #region Buttons
        internal Button btnSubmit = null;
        internal Button btnSendBack = null;
        internal Button btnComplete = null;
        internal Button btnUnlock = null;
        internal Button btnLock = null;
        #endregion
        #region Groupboxes
        internal GroupBox gbTypeReview = null;
        internal GroupBox gbReviewInfo = null;
        internal GroupBox gbSubReview = null;
        internal GroupBox gbComplete = null;
        #endregion

        public InputForm()
        {
            this.Load += new EventHandler(Form_load);
        }

        public override void CreateControls()
        {
            #region Buttons
            this.btnSubmit = FindControl(FC.ControlIDs.btnSubmit) as Button;
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
            this.btnSendBack = FindControl(FC.ControlIDs.btnSendBack) as Button;
            this.btnSendBack.Click += new EventHandler(btnSendBack_Click);
            this.btnComplete = FindControl(FC.ControlIDs.btnComplete) as Button;
            this.btnComplete.Click += new EventHandler(btnComplete_Click);
            this.btnUnlock = FindControl(FC.ControlIDs.btnUnlock) as Button;
            this.btnUnlock.Click += new EventHandler(btnUnlock_Click);
            this.btnLock = FindControl(FC.ControlIDs.btnLock) as Button;
            this.btnLock.Click += new EventHandler(btnLock_Click);
            #endregion
            #region Groupboxes
            this.gbReviewInfo = FindControl(FC.ControlIDs.gbReviewInfo) as GroupBox;
            this.gbSubReview = FindControl(FC.ControlIDs.gbSubReview) as GroupBox;
            this.gbTypeReview = FindControl(FC.ControlIDs.gbTypeReview) as GroupBox;
            this.gbComplete = FindControl(FC.ControlIDs.gbComplete) as GroupBox;
            #endregion
        }

        private void Form_load(object sender, EventArgs e)
        {
            FormHandling formHandling = new FormHandling(this);
            formHandling.FormLoad();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Submit submit = new Submit(this);
            submit.SubmitRequest();
        }

        private void btnSendBack_Click(object sender, EventArgs e)
        {
            SendBack SendBackButton = new SendBack(this);
            SendBackButton.HandleSendBack();
        }

        private void btnComplete_Click(object sender, EventArgs e)
        {
            Complete complete = new Complete(this);
            complete.ProcessComplete();
        }

        private void btnUnlock_Click(object sender, EventArgs e)
        {
            UnlockButton unlockButton = new UnlockButton(this);
            unlockButton.HandleUnlockButtonTest();
        }

        private void btnLock_Click(object sender, EventArgs e)
        {
            LockForm lockButton = new LockForm(this);
            lockButton.HandleLockFormButton();
        }
    }
}
