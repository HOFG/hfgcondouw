﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects;
using EllieMae.Encompass.BusinessObjects.Loans;

namespace HfgCondoUW
{
    public class SendBack
    {
        private InputForm inputForm { get; set; }
        private FormHandling formHandling { get; set; }
        private int resubCount;
        private int totalResubs;
        private int historyNumber = 0;

        public SendBack(InputForm inputForm)
        {
            this.formHandling = new FormHandling(inputForm);
            SetHistoryNumber();
        }

        private void SetHistoryNumber()
        {
            for (int index = 1; index <= 5; index++)
            {
                if (string.IsNullOrEmpty(EncompassApplication.CurrentLoan.Fields["cx.cuw.h." + index + ".3"].UnformattedValue) == true)
                {
                    historyNumber = index;
                    break;
                }
            }
        }

        public void HandleSendBack()
        {
            ProcessSendBack();
        }

        private void ProcessSendBack()
        {
            if (historyNumber > 0)
            {
                resubCount = Convert.ToInt32(EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.numResubs].Value);
                resubCount++;
                DateTime requestDateTime = ConvertTime.ConvertToArizonaTime(DateTime.Now);
                EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.subReviewByDate].Value = requestDateTime.ToShortDateString();
                EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.subReviewBy].Value = EncompassApplication.CurrentUser.FullName;
                EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.numResubs].Value = resubCount;
                EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.status].Value = "FALSE";
                MessageBox.Show("Submission review has successfully been sent back.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            EncompassApplication.CurrentLoan.Fields["cx.cuw.h." + historyNumber + ".3"].Value = EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.subReviewBy].Value;
            EncompassApplication.CurrentLoan.Fields["cx.cuw.h." + historyNumber + ".4"].Value = EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.subReviewByDate].Value;
            EncompassApplication.CurrentLoan.Fields["cx.cuw.h." + historyNumber + ".6"].Value = EncompassApplication.CurrentLoan.Fields[FC.Fields.Comments.condoComments].Value;
            ClearFields();
            formHandling.HandleSubmitRequestButton();
            formHandling.EnableForm();
            formHandling.DisableSendBackButton();
        }

        private void ClearFields()
        {
            EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submitTime].Value = null;

            // Condo team does not want to blank out fields. Just overrite them.

            //EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submittedBy].Value = null;
           // EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submittedByDate].Value = null;
           // EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.subReviewBy].Value = null;
           // EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.subReviewByDate].Value = null;

            EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.submissionType].Value = null;
            EncompassApplication.CurrentLoan.Fields[FC.Fields.Metrics.status].Value = "FALSE";
        }
    }
}
