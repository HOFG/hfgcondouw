﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HfgCondoUW
{
    public static class FC
    {
        public static class ControlIDs
        {
            public const string btnSubmit = "btnSubmit";
            public const string btnSendBack = "btnSendBack";
            public const string btnComplete = "btnComplete";
            public const string btnUnlock = "btnUnlock";
            public const string btnLock = "btnLock";
            public const string gbReviewInfo = "gbReviewInfo";
            public const string gbTypeReview = "gbTypeReview";
            public const string gbSubReview = "gbSubReview";
            public const string gbComplete = "gbComplete";
        }

        public static class Fields
        {
            public static class Metrics
            {
                public const string numResubs = "cx.cuw.num.resubs";
                public const string submitTime = "cx.cuw.submit.time";
                public const string submittedBy = "cx.cuw.submitted.by";
                public const string submittedByDate = "cx.cuw.submitted.by.date";
                public const string subReviewBy = "cx.cuw.reviewed.by";
                public const string subReviewByDate = "cx.cuw.reviewed.by.date";
                public const string completedBy = "cx.cuw.completed.by";
                public const string completedDate = "cx.cuw.completed.date";
                public const string status = "cx.cuw.m.status";
                public const string submissionType = "cx.cuw.submission.type";
            }

            public static class FormFields
            {
                public const string typeReview1 = "cx.cuw.tr.1";
                public const string typeReview2 = "cx.cuw.tr.2";
                public const string typeReview3 = "cx.cuw.tr.3";
                public const string typeReview4 = "cx.cuw.tr.4";
                public const string typeReview5 = "cx.cuw.tr.5";
                public const string typeReview6 = "cx.cuw.tr.6";
                public const string typeReview7 = "cx.cuw.tr.7";
                public const string typeReview8 = "cx.cuw.tr.8";
                public const string typeReview9 = "cx.cuw.tr.9";
                public const string typeReview10 = "cx.cuw.tr.10";
                public const string typeReview11 = "cx.cuw.tr.11";
                public const string typeReview12 = "cx.cuw.tr.12";
            }

            public static class History
            {
                public const string h1Dot1 = "cx.cuw.h.1.1";
                public const string h1Dot2 = "cx.cuw.h.1.2";
                public const string h1Dot3 = "cx.cuw.h.1.3";
                public const string h1Dot4 = "cx.cuw.h.1.4";
                public const string h1Dot5 = "cx.cuw.h.1.5";
                public const string h1Dot6 = "cx.cuw.h.1.6";

                public const string h2Dot1 = "cx.cuw.h.2.1";
                public const string h2Dot2 = "cx.cuw.h.2.2";
                public const string h2Dot3 = "cx.cuw.h.2.3";
                public const string h2Dot4 = "cx.cuw.h.2.4";
                public const string h2Dot5 = "cx.cuw.h.2.5";
                public const string h2Dot6 = "cx.cuw.h.2.6";

                public const string h3Dot1 = "cx.cuw.h.3.1";
                public const string h3Dot2 = "cx.cuw.h.3.2";
                public const string h3Dot3 = "cx.cuw.h.3.3";
                public const string h3Dot4 = "cx.cuw.h.3.4";
                public const string h3Dot5 = "cx.cuw.h.3.5";
                public const string h3Dot6 = "cx.cuw.h.3.6";

                public const string h4Dot1 = "cx.cuw.h.4.1";
                public const string h4Dot2 = "cx.cuw.h.4.2";
                public const string h4Dot3 = "cx.cuw.h.4.3";
                public const string h4Dot4 = "cx.cuw.h.4.4";
                public const string h4Dot5 = "cx.cuw.h.4.5";
                public const string h4Dot6 = "cx.cuw.h.4.6";

                public const string h5Dot1 = "cx.cuw.h.5.1";
                public const string h5Dot2 = "cx.cuw.h.5.2";
                public const string h5Dot3 = "cx.cuw.h.5.3";
                public const string h5Dot4 = "cx.cuw.h.5.4";
                public const string h5Dot5 = "cx.cuw.h.5.5";
                public const string h5Dot6 = "cx.cuw.h.5.6";

                public const string h6Dot1 = "cx.cuw.h.6.1";
                public const string h6Dot2 = "cx.cuw.h.6.2";
                public const string h6Dot3 = "cx.cuw.h.6.3";
                public const string h6Dot4 = "cx.cuw.h.6.4";
                public const string h6Dot5 = "cx.cuw.h.6.5";
                public const string h6Dot6 = "cx.cuw.h.6.6";

                public const string h7Dot1 = "cx.cuw.h.7.1";
                public const string h7Dot2 = "cx.cuw.h.7.2";
                public const string h7Dot3 = "cx.cuw.h.7.3";
                public const string h7Dot4 = "cx.cuw.h.7.4";
                public const string h7Dot5 = "cx.cuw.h.7.5";
                public const string h7Dot6 = "cx.cuw.h.7.6";

                //public const string h8Dot1 = "cx.cuw.h.8.1";
                //public const string h8Dot2 = "cx.cuw.h.8.2";
                //public const string h8Dot3 = "cx.cuw.h.8.3";
                //public const string h8Dot4 = "cx.cuw.h.8.4";
                //public const string h8Dot5 = "cx.cuw.h.8.5";
                //public const string h8Dot6 = "cx.cuw.h.8.6";
            }
            public static class Comments
            {
                public const string comments = "cx.cuw.comments";
                public const string condoComments = "cx.cuw.condo.comments";
            }
        }
    }
}
